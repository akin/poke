# PROJECT_NAME is a word, that CMake has reserved for its own internal use (evil). So application_name is used instead.
set(APPLICATION_NAME "Poke")
set(APPLICATION_VERSION "0.0.1")
set(APPLICATION_CONFIG_FILE "config.json")

set(DEFAULT_COMPILE_FLAGS "-fno-rtti -fno-exceptions")
if (CMAKE_GENERATOR MATCHES "Visual Studio")
	# msvc is retarded.
	# tried to read their documentation on the flags.. now I feel even more dummer than before reading their documentation.
	# if there was a way to do it simple, msvc team will make it complicated, and add some satanistic rituals in it.
	set(DEFAULT_COMPILE_FLAGS "/GR- /EHsc")
endif()

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Having PROJECT_ here is dangerous, but again, also having CMake reserve some random words is just plain evil.
set(PROJECT_ROOT "${CMAKE_CURRENT_LIST_DIR}")
set(PROJECT_LIBS "${PROJECT_ROOT}/libs")

add_definitions(-DAPPLICATION_NAME="${APPLICATION_NAME}")
add_definitions(-DAPPLICATION_VERSION="${APPLICATION_VERSION}")
add_definitions(-DAPPLICATION_CONFIG_FILE="${APPLICATION_CONFIG_FILE}")
