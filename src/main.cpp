#include <iostream>
#include <string>
#include <image.h>
#include <imageio.h>
#include <converter.h>
#include <types.h>

#ifndef APPLICATION_CONFIG_FILE
	#define APPLICATION_CONFIG_FILE "config.json"
#endif

int main(int argc, char *argv[])
{
	int ret = EXIT_FAILURE;

	Image<RGBA_uint8> picture;
	std::string path = "test.jpg";
	if (!ImageIO::load(picture, path))
	{
		std::cout << "Failed to load image '" << path << "'." << std::endl;
	}
	/*
	Image<RGBA_uint8> copy;
	if (!Converter::copy(picture, copy))
	{
		std::cout << "Failed to copy image '" << path << "'." << std::endl;
	}

	if (!ImageIO::save(ImageFileFormat::png, picture, path + "2.png"))
	{
		std::cout << "Failed to load image '" << path << "'." << std::endl;
	}
	*/

	CubeMap<Image<RGBA_uint8>> cube;
	if (!Converter::convert(ImageContentType::dual_fisheye, picture, cube))
	{
		std::cout << "Failed to convert image '" << path << "'." << std::endl;
	}

	{
		// up
		auto& up = cube.get(Side::up);
		if (!ImageIO::save(ImageFileFormat::png, up, path + ".up.png"))
		{
			std::cout << "Failed to load image '" << path << ".up'." << std::endl;
		}
		// down
		auto& down = cube.get(Side::down);
		if (!ImageIO::save(ImageFileFormat::png, down, path + ".down.png"))
		{
			std::cout << "Failed to load image '" << path << ".down'." << std::endl;
		}
		auto& left = cube.get(Side::left);
		if (!ImageIO::save(ImageFileFormat::png, left, path + ".left.png"))
		{
			std::cout << "Failed to load image '" << path << ".left'." << std::endl;
		}
		auto& right = cube.get(Side::right);
		if (!ImageIO::save(ImageFileFormat::png, right, path + ".right.png"))
		{
			std::cout << "Failed to load image '" << path << ".right'." << std::endl;
		}
		auto& front = cube.get(Side::front);
		if (!ImageIO::save(ImageFileFormat::png, front, path + ".front.png"))
		{
			std::cout << "Failed to load image '" << path << ".front'." << std::endl;
		}
		auto& back = cube.get(Side::back);
		if (!ImageIO::save(ImageFileFormat::png, back, path + ".back.png"))
		{
			std::cout << "Failed to load image '" << path << ".back'." << std::endl;
		}
	}

	std::cout << "Hello world" << std::endl;

	return ret;
}
