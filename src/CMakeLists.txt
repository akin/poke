include_directories(".")

set(CURRENT_HEADERS)
set(CURRENT_SOURCES)
file(GLOB CURRENT_HEADERS 
	${CMAKE_CURRENT_LIST_DIR}/*.h
)
file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/*.c
	${CMAKE_CURRENT_LIST_DIR}/*.inl
	${CMAKE_CURRENT_LIST_DIR}/*.h
)

add_executable(${APPLICATION_NAME} 
    ${CURRENT_HEADERS}
    ${CURRENT_SOURCES}
)
target_link_libraries(
    ${APPLICATION_NAME} 
    ${PROJECT_LIBRARIES}
)
set_target_properties(${APPLICATION_NAME} PROPERTIES COMPILE_FLAGS ${DEFAULT_COMPILE_FLAGS})

# copy library dll's
FOREACH(DLL_ITEM ${PROJECT_DLL_LIST})
  add_custom_command(TARGET ${APPLICATION_NAME} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different "${DLL_ITEM}" $<TARGET_FILE_DIR:${APPLICATION_NAME}>)
ENDFOREACH()

# Copy config file from project directory to "build working directory" on build time.
add_custom_command(TARGET ${APPLICATION_NAME} POST_BUILD 
    COMMAND ${CMAKE_COMMAND} -E copy_if_different 
        "${PROJECT_SOURCE_DIR}/config.json"    
        "$<TARGET_FILE_DIR:${APPLICATION_NAME}>/../") 

set_target_properties(${APPLICATION_NAME} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
