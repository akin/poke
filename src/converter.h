#pragma once
#ifndef CONVERTER_H_
#define CONVERTER_H_

#include <types.h>
#include <image.h>
#include <assert.h>
#include <sampler.h>

namespace Converter
{
	template <typename CType>
	bool copy(Image<CType>& source, Image<CType>& target)
	{
		auto size = source.getSize();
		target.resize(size);

		Sampler2D<CType> sourceSampler(source);
		Sampler2D<CType> targetSampler(target);

		Vec2i position;

		Pixel<RGBA_uint8> color;
		color.r = 0xFF;
		color.g = 0x00;
		color.b = 0x00;
		color.a = 0xFF;

		for (int32_t height = 0; height < size.y; ++height)
		{
			for (int32_t width = 0; width < size.x; ++width)
			{
				position = { width, height };
				targetSampler.write(position, targetSampler.read(position));
			}
		}

		return true;
	}

	template <typename CType>
	bool convert(ImageContentType content, Image<CType>& image, CubeMap<Image<CType>>& cube)
	{
		std::unique_ptr<SamplerDirection<CType>> sourceSampler;

		int32_t size = 0;
		switch (content)
		{
			case ImageContentType::dual_fisheye:
			{
				size = image.getSize().y;
				sourceSampler.reset(new DualFishEyeSampler<CType>(image));
				break;
			}
			default:
				break;
		}

		if (sourceSampler.get() == nullptr)
		{
			assert(false && "No support for this format");
			return false;
		}

		const float full = size;
		const float inv = 1.0f / full;

		Vec3 direction;
		Vec2i position;
		// Go through cubemap sides, & sample the image, and write into cubemap
		// up
		{
			direction.y = 1.0f;

			auto& target = cube.get(Side::up);
			target.resize({size, size});

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.z = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.x = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		// down
		{
			direction.y = -1.0f;

			auto& target = cube.get(Side::down);
			target.resize({ size, size });

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.z = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.x = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		// right
		{
			direction.x = 1.0f;

			auto& target = cube.get(Side::right);
			target.resize({ size, size });

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.y = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.z = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		// left
		{
			direction.x = -1.0f;

			auto& target = cube.get(Side::left);
			target.resize({ size, size });

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.y = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.z = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		// front
		{
			direction.z = 1.0f;

			auto& target = cube.get(Side::front);
			target.resize({ size, size });

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.y = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.x = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		// back
		{
			direction.z = -1.0f;

			auto& target = cube.get(Side::back);
			target.resize({ size, size });

			Sampler2D<CType> targetSampler(target);

			for (int32_t height = 0; height < size; ++height)
			{
				direction.y = height * inv - 0.5f;
				for (int32_t width = 0; width < size; ++width)
				{
					position = { width, height };
					direction.x = width * inv - 0.5f;
					targetSampler.write(position, sourceSampler->readDirection(direction));
				}
			}
		}
		return true;
	}
};

#endif // CONVERTER_H_