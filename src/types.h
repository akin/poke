#pragma once
#ifndef TYPES_H_
#define TYPES_H_

#include <stdint.h>

template <typename CType>
class RGBABase
{
public:
	CType r;
	CType g;
	CType b;
	CType a;
};

template <typename CType>
class Vec2Base
{
public:
	Vec2Base(CType x = 0, CType y = 0)
	: x(x), y(y) 
	{
	}

	template<typename CType2>
	explicit Vec2Base(Vec2Base<CType2> other)
	: x(other.x), y(other.y) 
	{
	}

	CType x,y;

	void normalize()
	{
		CType len = std::sqrt(x*x + y*y);
		x /= len;
		y /= len;
	}
};

template <typename CType>
class Vec3Base
{
public:
	CType x,y,z;

	void normalize()
	{
		CType len = std::sqrt(x*x + y*y + z*z);
		x /= len;
		y /= len;
		z /= len;
	}
};

template <typename CType>
class Vec4Base
{
public:
	CType x,y,z,w;

	void normalize()
	{
		CType len = std::sqrt(x*x + y*y + z*z + w*w);
		x /= len;
		y /= len;
		z /= len;
		w /= len;
	}
};

enum class ImageContentType
{
	simple,
	fisheye,
	dual_fisheye
};

enum class ImageFileFormat
{
	jpg,
	png,
	bmp,
	tga,
	hdr,
	max
};

using Vec2i = Vec2Base<int32_t>;
using Vec3i = Vec3Base<int32_t>;
using Vec4i = Vec4Base<int32_t>;
using Vec2 = Vec2Base<float>;
using Vec3 = Vec3Base<float>;
using Vec4 = Vec4Base<float>;

using RGBA_uint8 = RGBABase<uint8_t>;
using RGBA_float = RGBABase<float>;

#endif // TYPES_H_