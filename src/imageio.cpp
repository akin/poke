#include "imageio.h"

#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <assert.h>


bool ImageIO::load(Image<RGBA_uint8>& image, std::string path)
{
	std::vector<uint8_t> data;

	std::ifstream file(path, std::ios::binary | std::ios::ate);

	if (!file.good())
	{
		return false;
	}

	std::streamsize size = file.tellg();
	file.seekg(0, std::ios::beg);
	data.resize(size);

	file.read((char*)data.data(), size);

	if (!file.good())
	{
		return false;
	}

	return load(image, data);
}

bool ImageIO::load(Image<RGBA_uint8>& image, const std::vector<uint8_t>& data)
{
	int len, x, y, comp;
	int ret = stbi_info_from_memory(data.data(), data.size(), &x, &y, &comp);

	if (ret <= 0)
	{
		return false;
	}

	auto rawimage = stbi_load_from_memory(data.data(), data.size(), &x, &y, &comp, 4);

	if (rawimage == NULL)
	{
		return false;
	}
	image.set({ static_cast<int32_t>(x), static_cast<int32_t>(y) }, rawimage);
	stbi_image_free(rawimage);

	return true;
}

bool ImageIO::save(ImageFileFormat format, Image<RGBA_uint8>& image, std::string path)
{
	/*
int stbi_write_png(char const *filename, int w, int h, int comp, const void *data, int stride_in_bytes);
int stbi_write_bmp(char const *filename, int w, int h, int comp, const void *data);
int stbi_write_tga(char const *filename, int w, int h, int comp, const void *data);
int stbi_write_jpg(char const *filename, int w, int h, int comp, const void *data, int quality);
int stbi_write_hdr(char const *filename, int w, int h, int comp, const float *data);
	*/
	int ret = 0;
	auto size = image.getSize();
	void *data = image.data();
	int comp = 4;
	switch (format)
	{
		case(ImageFileFormat::png):
		{
			ret = stbi_write_png(path.c_str(), size.x, size.y, comp, data, size.x * comp);
			break;
		}
		case(ImageFileFormat::jpg):
		{
			ret = stbi_write_jpg(path.c_str(), size.x, size.y, comp, data, 95);
			break;
		}
		default:
			break;
	}
	if (ret <= 0)
	{
		return false;
	}

	return true;
}

