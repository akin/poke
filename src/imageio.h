#pragma once
#ifndef IMAGEIO_H_
#define IMAGEIO_H_

#include <vector>
#include <types.h>
#include <image.h>
#include <string>

namespace ImageIO
{
	bool load(Image<RGBA_uint8>& image, std::string path);
	bool load(Image<RGBA_uint8>& image, const std::vector<uint8_t>& data);

	bool save(ImageFileFormat format, Image<RGBA_uint8>& image, std::string path);
};

#endif // IMAGEIO_H_