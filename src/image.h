#pragma once
#ifndef IMAGE_H_
#define IMAGE_H_

#include <memory>
#include <types.h>
#include <assert.h>

template <typename... Content>
class Pixel : public Content...
{
public:
};

template <typename... Content>
class Image
{
private:
	std::unique_ptr<Pixel<Content...>[]> m_data;
	Vec2i m_size;
	size_t m_pixelSize;
	size_t m_pixelCount;
	size_t m_bufferSize;
public:
	Image()
	: m_pixelSize(sizeof(Pixel<Content...>))
	{
	}

	Image(Vec2i size)
	: m_pixelSize(sizeof(Pixel<Content...>))
	{
		resize(size);
	}

	void reset(Vec2i size, void *data)
	{
		m_data.reset(nullptr);
		m_size = 0;
		m_pixelCount = 0;
		m_bufferSize = 0;
	}

	void resize(Vec2i size)
	{
		m_size = size;
		m_pixelCount = size.x * size.y;
		m_bufferSize = m_pixelSize * m_pixelCount;
		m_data.reset(new Pixel<Content...>[m_pixelCount]);
	}

	void set(Vec2i size, void *data)
	{
		resize(size);
		void *target = m_data.get();
		memcpy(target, data, m_bufferSize);
	}

	Pixel<Content...>* data()
	{
		assert(valid());
		return m_data.get();
	}

	bool valid() const
	{
		return m_data.get() != nullptr;
	}

	Vec2i getSize() const
	{
		return m_size;
	}

	size_t getByteSize() const
	{
		return m_bufferSize;
	}

	size_t getPixelSize() const
	{
		return m_pixelSize;
	}

	size_t getPixelCount() const
	{
		return m_pixelCount;
	}
};

enum class Side : uint8_t
{
	up,
	down,
	left,
	right,
	back,
	front,
	max
};

template <typename CType>
class CubeMap
{
private:
	CType m_side[Side::max];
public:
	CType & get(Side side)
	{
		return m_side[static_cast<uint8_t>(side)];
	}
};

#endif // ACTION_H_