#pragma once
#ifndef SAMPLER_H_
#define SAMPLER_H_

#include <image.h>
#include <types.h>
#include <assert.h>

template <typename... Content>
class Sampler2D
{
protected:
	Image<Content...>& m_image;
	Vec2i m_size;
public:
	Sampler2D(Image<Content...>& image)
	: m_image(image)
	, m_size(image.getSize())
	{
	}
public:
	virtual ~Sampler2D() = default;

	const Pixel<Content...>& read(Vec2i position)
	{
		assert(m_size.x > 0 && m_size.y > 0 && "Image size wrong!");
		return m_image.data()[position.y * m_size.x + position.x];
	}

	const Pixel<Content...>& read(Vec2 position)
	{
		return read(Vec2i{ static_cast<int32_t>(position.x) ,static_cast<int32_t>(position.y) });
	}

	void write(Vec2i position, const Pixel<Content...>& pixel)
	{
		assert(m_size.x > 0 && m_size.y > 0 && "Image size wrong!");
		size_t pos = position.y * m_size.x + position.x;

		assert(pos < m_image.getPixelCount());

		Pixel<Content...> *data = m_image.data();

		// TODO, Memory referenced incorrectly..
		// About at halfway of image it blurbs errors
		// Exception thrown at 0x00007FF94D4D17AB (vcruntime140d.dll) in Poke.exe: 0xC0000005: Access violation reading location 0x000002943ED99C50.
		memcpy(&(data[pos]), &pixel, sizeof(Pixel<Content...>));
	}

	void write(Vec2 position, const Pixel<Content...>& pixel)
	{
		write(Vec2i{ static_cast<int32_t>(position.x) ,static_cast<int32_t>(position.y) }, pixel);
	}
};

template <typename... Content>
class SamplerDirection : public Sampler2D<Content...>
{
public:
	SamplerDirection(Image<Content...>& image)
	: Sampler2D(image)
	{
	}

	virtual ~SamplerDirection() = default;

	virtual const Pixel<Content...>& readDirection(Vec3 direction) = 0;
};

template <typename... Content>
class DualFishEyeSampler : public SamplerDirection<Content...>
{
private:
	int32_t m_size;
public:
	DualFishEyeSampler(Image<Content...>& image)
	: SamplerDirection(image)
	{
		m_size = m_image.getSize().y;
	}

	const Pixel<Content...>& readDirection(Vec3 direction) override
	{
		direction.normalize();

		// referencing to the other image, which is inversed.
		if (direction.z < 0.0f)
		{
			direction.x *= -1;
		}

		// all positive!
		direction.x = (direction.x + 1.0f) * 0.5f;
		direction.y = (direction.y + 1.0f) * 0.5f;

		Vec2 position = { direction.x * m_size , direction.y * m_size };

		// offset the image to the second image..
		if (direction.z < 0.0f)
		{
			position.x += m_size;
		}

		return read(position);
	}
};


#endif // SAMPLER_H_