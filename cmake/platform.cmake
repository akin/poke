
## Platform detection part
## This library provides platform detection service
set(PLATFORM_NAME "NONE")
set(PLATFORM_VERSION "0.0.1")
if(UNIX)
    # Linux, Bsd, Apple etc.
    if(APPLE)
        set(PLATFORM_NAME "MAC")
        set(MAC 1)
    else()
        set(PLATFORM_NAME "LINUX")
        set(LINUX 1)
    endif()
elseif(WIN32)
    set(PLATFORM_NAME "WINDOWS")
    set(WINDOWS 1)
endif()

## It also leaks things to C/C++ side
add_definitions("-DPLATFORM_${PLATFORM_NAME}")
add_definitions(-DPLATFORM_NAME="${PLATFORM_NAME}")
add_definitions(-DPLATFORM_VERSION="${PLATFORM_VERSION}")