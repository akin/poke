cmake_minimum_required(VERSION 3.7)

set(LorCore_PATH "${CMAKE_CURRENT_LIST_DIR}/../core")
set(LorCore_INCLUDES "${LorCore_PATH}/inc")

include_directories("${LorCore_INCLUDES}")
