cmake_minimum_required(VERSION 3.7)

include(LorCore)

set(LorPlatform_PATH "${CMAKE_CURRENT_LIST_DIR}/../platform")
set(LorPlatform_INCLUDES "${LorPlatform_PATH}/inc")

include_directories(${LorPlatform_INCLUDES})
